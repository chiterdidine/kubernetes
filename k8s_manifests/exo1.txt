# les commandes kubectl 

kubectl run pod1  --images=redis:6-alpine

kubectl logs pod1

kubectl exec -it po/pod1 -- redis-cli 

kubectl delete po/pod1

kubectl create deployment d1 --image=httpd:2.4-alpine --replicas=2

kubectl get pods

kubectl describe po/d1


kubectl expose deploy/d1 --type=NodePort --port=80


kubectl get svc 


kubectl delete deploy/d1 svc/apache


 
